package run;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import impl.FileCsvDataRecordDecoder;
import model.DataRecord;
import model.FileCsvDataRecordDecoderBuilder;

public class Main {

//	private static final String sepoaratoreFields = ",";
	/**
	 * Creo delle costanti con i relativi percorsi dei File da leggere
	 */
	private static final String PATH_FILE_MULTILINE = "C:\\Users\\fabio.barbanera\\git\\datarecord2\\DataRecord2\\src\\main\\resources\\ElencoRecordsMultiLine.txt";
//	private static final String PATH_FILE_INLINE = "C:\\Users\\fabio.barbanera\\git\\datarecord2\\DataRecord2\\src\\main\\resources\\ElencoRecordsInLine.txt";

	public static void main(String[] args) throws FileNotFoundException, IOException, NoSuchMethodException,
			SecurityException, IllegalAccessException, IllegalArgumentException, InvocationTargetException {

		/*
		 * Creo un decoder passando al metodo statico build il percorso del file Così da
		 * controllare quale decoder usare per leggere il File
		 */
		FileCsvDataRecordDecoder decoder = FileCsvDataRecordDecoderBuilder.build(PATH_FILE_MULTILINE);

		/*
		 * Con il primo ciclo while mi prendo il record successivo, con il secondo ciclo
		 * while mi prendo del record ottenuto il prossimo field
		 */
		while (decoder.hasNext()) {
			DataRecord dataRecordSuccessivo = decoder.nextRecord();
			while (dataRecordSuccessivo.hasNext()) {
				System.out.println(dataRecordSuccessivo.nextField());
			}
			System.out.println("-----------Record Successivo---------");
		}
		System.err.println("Records Terminati!");
	}
}
