package model;

public interface DataRecordDecoder{

	public DataRecord nextRecord();
	
	public boolean hasNext();
	
	
}
