package model;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import impl.FileMultiLineCsvDataRecordDecoder;
import impl.FileCsvDataRecordDecoder;
import impl.FileInLineCsvDataRecordDecoder;

public class FileCsvDataRecordDecoderBuilder {

	private static List<Class<?>> classes = new ArrayList<Class<?>>();
	private FileCsvDataRecordDecoderBuilder() {
	}

	public static FileCsvDataRecordDecoder build(String PATH) throws IOException, NoSuchMethodException,
			SecurityException, IllegalAccessException, IllegalArgumentException, InvocationTargetException {

		/*
		 * Qua potrei farmi una lista di classi di tipo FileCsvDataRecordDecoder
		 * Scorrerla con un ciclo e in base al ritorno del metodo checkCompatibility
		 * posso nel caso istanziarla o recuperarmi l'istanza del decoder se già è
		 * esistente
		 */

		classes.add(FileInLineCsvDataRecordDecoder.class);
		classes.add(FileMultiLineCsvDataRecordDecoder.class);
		FileCsvDataRecordDecoder decoder = null;

		/*
		 * Con un ciclo for sulla lista che contiene le classi
		 */
		for (Class<?> clazz : classes) {
			
			/*
			 * Il metodo per essere letto dalla classe deve essere statico.
			 * Se recupero il metodo dalla classe in questo modo deve essere statico.
			 * Altrimenti dovrei perndermi il costruttore della classe,
			 * costruirmi l'oggetto di tipo decoder e poi invocare il metodo!
			 */
			/*
			 * Mi prendo il metodo checkCompatibility che controlla se tale classe può leggere 
			 * il tipo di file passato oppure no
			 */
			Method checkCompatibility = clazz.getDeclaredMethod("checkCompatibility", String.class);
			boolean compatibility = (boolean) checkCompatibility.invoke(clazz, PATH);

			/*
			 * Se esiste una classe che può leggere quel file
			 */
			if (compatibility == true) {
				/*
				 * mi prendo il metodo che crea o ritorna il decoder per leggere il file
				 */
				Method creaORitornaDecoder = clazz.getDeclaredMethod("creaORitornaDecoder");
				/*
				 * Invoco il metodo
				 */
				decoder = (FileCsvDataRecordDecoder) creaORitornaDecoder.invoke(clazz);
				/*
				 * Leggo il file
				 */
				decoder.leggiFile(PATH);
				/*
				 * ritorno il decoder da usare nel Main
				 */
				return decoder;
			}
		}
		/*
		 * Se non c'è un decoder per leggere il file il cui percorso è stato passato 
		 * nell'invocazione del metodo che controlla la compatibilità, sollevo un'eccezione
		 */
		throw new IllegalArgumentException("Decoder non trovato per leggere questo File");
		

//		if(FileInLineCsvDataRecordDecoder.checkCompatibility(PATH)==true) {
//			FileInLineCsvDataRecordDecoder decoder = FileInLineCsvDataRecordDecoder.creaORitornaInLineCsvDecoder();
//			decoder.leggiFile(PATH);
//			return FileInLineCsvDataRecordDecoder.creaORitornaInLineCsvDecoder();
//		}
//		
//		if(FileMultiLineCsvDataRecordDecoder.checkCompatibility(PATH)==true) {
//			FileMultiLineCsvDataRecordDecoder decoder = FileMultiLineCsvDataRecordDecoder.creaORitornaMultiLineCsvDecoder();
//			decoder.leggiFile(PATH);
//			return FileMultiLineCsvDataRecordDecoder.creaORitornaMultiLineCsvDecoder();
//		}
//		
//		throw new IllegalArgumentException("Decoder non trovato per la lettura del file passato!");

	}

//	public static FileCsvDataRecordDecoder controllaCompatibilitaDecoder(String PATH) throws IOException{
//
//		String rigaLetta = null;
//
//		try {
//			leggiDocumento = new BufferedReader(new FileReader(PATH));
//		} catch (FileNotFoundException e) {
//			System.err.println("Lettura File non riuscita!");
//		}
//
//		/*
//		 * Se è inline ovviamente ha una sola riga e usando leggidocumento(BufferedReader) più di una
//		 * volta se il file è su più di una linea sicuramente rigaletta ha valore
//		 * diverso da null altrimenti se è su una sola linea la seconda volta non
//		 * leggendo nulla il valore di rigaletta sarà null
//		 */
//		rigaLetta = leggiDocumento.readLine();
//		rigaLetta = leggiDocumento.readLine();
//
//		if (rigaLetta == null) {
//			FileInLineCsvDataRecordDecoder inLineDecoder = FileInLineCsvDataRecordDecoder.creaInLineDecoder();
//			inLineDecoder.leggiFile(PATH);
//			return inLineDecoder;
//		} else {
//			FileMultiLineCsvDataRecordDecoder multiLineDecoder = FileMultiLineCsvDataRecordDecoder.creaCsvDecoder();
//			multiLineDecoder.leggiFile(PATH);
//			return multiLineDecoder;
//		}
//	}
}
