package model;

import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;

public class DataRecord {

	private List<DataField> dataFieldsList;
	private Iterator<DataField> iteratoreDataFields;

	public DataRecord(List<DataField> dataFieldsList) {
		this.dataFieldsList = dataFieldsList;
		this.iteratoreDataFields = dataFieldsList.iterator();
	}

	public DataField nextField() {

		if (iteratoreDataFields.hasNext()) {
			return iteratoreDataFields.next();
		} else {
			throw new NoSuchElementException("DataFields di questo DataRecord finiti");
		}
	}

	public boolean hasNext() {

		if (iteratoreDataFields.hasNext()) {
			return true;
		} else {
			return false;
		}
	}

	@Override
	public String toString() {
		
			 return "Data Records{" + dataFieldsList + "}\n";
		
	}
}
