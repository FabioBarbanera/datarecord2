package model;

public class DataField {

	String nome;
	String valore;

	public DataField(String nome, String valore) {
		super();
		this.nome = nome;
		this.valore = valore;
	}

	public String getNome() {
		return nome;
	}

	public String getValore() {
		return valore;
	}

	@Override
	public String toString() {
		return "DataField ["+nome+ " : "+ valore+"]";
	}

}
