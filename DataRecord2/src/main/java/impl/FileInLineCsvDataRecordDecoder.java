package impl;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.List;

public class FileInLineCsvDataRecordDecoder extends FileCsvDataRecordDecoder {

	private static FileInLineCsvDataRecordDecoder getIstance;

	/**
	 * Rendo privato il costruttore per usare il pattern Singleton
	 */
	private FileInLineCsvDataRecordDecoder() {
	}

	/**
	 * Metodo per creare un oggetto FileMultiLineCsvDataRecordDecoder o se già
	 * esiste una sua istanza per ritornarla
	 * 
	 * @return
	 */
	public static FileInLineCsvDataRecordDecoder creaORitornaDecoder() {

		if (getIstance == null) {
			return getIstance = new FileInLineCsvDataRecordDecoder();
		} else {
			System.err.println("Può esistere una sola istanza di " + FileInLineCsvDataRecordDecoder.class.getName());
		}
		return getIstance;
	}

	/**
	 * Implementazione del metodo astratto della classe padre in cui mi prendo i
	 * records
	 */
	public List<String> estraiDati(BufferedReader leggiDocumento) throws IOException {

		String rigaLetta = leggiDocumento.readLine();
		String[] datiRigaLettaSplittati = rigaLetta.split(";");

		for (int i = 0; i < datiRigaLettaSplittati.length; i++) {
			records.add(datiRigaLettaSplittati[i]);
		}

		return records;
	}

	/**
	 * Metodo per controllare, passando il percorso del file, se questo decoder è in
	 * grado di leggerlo
	 */
	public static boolean checkCompatibility(String PATH) throws IOException {

		int contatoreRighe = 0;

		try (BufferedReader leggiDocumento = new BufferedReader(new FileReader(PATH))) {
			while (leggiDocumento.readLine() != null) {
				++contatoreRighe;
			}
		}
		if (contatoreRighe > 1) {
			return false;
		} else {
			return true;
		}
	}

}
