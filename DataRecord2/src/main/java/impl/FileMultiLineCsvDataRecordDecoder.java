package impl;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.List;

public class FileMultiLineCsvDataRecordDecoder extends FileCsvDataRecordDecoder {

	private static FileMultiLineCsvDataRecordDecoder getIstance;

	/**
	 * Rendo privato il costruttore per usare il pattern Singleton
	 */
	private FileMultiLineCsvDataRecordDecoder() {}

	/**
	 * Metodo per creare un oggetto FileMultiLineCsvDataRecordDecoder 
	 * o se già esiste una sua istanza per ritornare quella esistente
	 * @return
	 */
	public static FileMultiLineCsvDataRecordDecoder creaORitornaDecoder() {

		if (getIstance == null) {
			return getIstance = new FileMultiLineCsvDataRecordDecoder();
		} else {
			System.err.println("Può esistere una sola istanza di " + FileMultiLineCsvDataRecordDecoder.class.getName());
			return getIstance;
		}
	}

	/**
	 * Implementazione del metodo astratto della classe padre in cui mi prendo 
	 * i records
	 */
	public List<String> estraiDati(BufferedReader leggiDocumento) throws IOException {

		String rigaLetta = null;
		while ((rigaLetta = leggiDocumento.readLine()) != null) {
			records.add(rigaLetta);
		}
		return records;
	}

	/**
	 * Metodo per controllare, passando il percorso del file, se questo decoder è in
	 * grado di leggerlo
	 */
	public static boolean checkCompatibility(String PATH) throws FileNotFoundException, IOException {

		int contatoreRighe = 0;

		try (BufferedReader leggiDocumento = new BufferedReader(new FileReader(PATH))) {
			while (leggiDocumento.readLine() != null) {
				++contatoreRighe;
			}
		}
		if (contatoreRighe > 1) {
			return true;
		} else {
			return false;
		}
	}
}
