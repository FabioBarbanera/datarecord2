package impl;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;
import model.DataField;
import model.DataRecord;
import model.DataRecordDecoder;

public abstract class FileCsvDataRecordDecoder implements DataRecordDecoder {

	/**
	 * creo una stringa regex per controllare se il formato dei records è giusto
	 */
	private static String stringaMatch = "[A-Za-z]+[,][A-Za-z]+[,][0-9]+[,][A-Za-z]+[,][0-9]+[,][A-Za-z]+";
	private List<DataRecord> dataRecordsList = new ArrayList<DataRecord>();
	private List<String> listaRecordsNonCorretti = new ArrayList<String>();
	protected List<String> records = new ArrayList<String>();
	private Iterator<DataRecord> iteratoreDataRecords;

	/**
	 * Metodo per creare i DataFields ed i DataRecords
	 * 
	 * @param path
	 */
	public void leggiFile(String path) {

		BufferedReader leggiDocumento = null;
		String[] intestazioni;
		String[] recordArrayRecordCorretto = null;
		List<DataField> dataFieldsList = null;

		try {
			/*
			 * Creo un BufferedReader a cui passo un file reader a cui a sua volta passo il
			 * percorso del file da leggere
			 */
			leggiDocumento = new BufferedReader(new FileReader(path));

			/*
			 * Implementazione diversa: in base alla tipologia di file che viene letto il
			 * decoder dovrà recuperare i dati in maniera diversa fino a che non avrà la
			 * lista dei record così da poter contntinuare ad usare l'algoritmica della
			 * classe padre(FileCsvDataRecordDecoder)
			 */
			estraiDati(leggiDocumento);
			/*
			 * 
			 */

			/*
			 * Una volta recuperati i records, la prima posizione della lista sicuramente
			 * contiene i fields dell'intestazione
			 */
			intestazioni = records.get(0).split(",");

			/*
			 * Con un ciclo for sulla lunghezza della lista che contiene i records
			 */
			for (int i = 1; i < records.size(); i++) {

				/*
				 * Mi creo una lista ad ogni giro che contenga i singoli fields del record a
				 * quel giro con la relativa intestazione
				 */
				dataFieldsList = new ArrayList<DataField>();

				/*
				 * un booleano per vedere se la regex di controllo conferma l'esattezza del
				 * formato del record
				 */
				boolean matchRegex = records.get(i).matches(stringaMatch);

				/*
				 * Se il formato del record è corretto
				 */
				if (matchRegex) {
					/*
					 * splitto il record nei singoli fields e lo metto in un array
					 */
					recordArrayRecordCorretto = records.get(i).split(",");

					/*
					 * Con un ciclo for sulla lunghezza dell'array contenente il record splittato
					 * (quindi i singoli fields)
					 */
					for (int j = 0; j < recordArrayRecordCorretto.length; j++) {
						/*
						 * Creo il DataField con l'intestazione ed il relativo field
						 */
						DataField dataField = new DataField(intestazioni[j], recordArrayRecordCorretto[j]);
						/*
						 * Aggiungo il DataField alla lista di dataFields
						 */
						dataFieldsList.add(dataField);
					}
					/*
					 * Con la lista di dataFields posso creare il DataRecord
					 */
					DataRecord dataRecord = new DataRecord(dataFieldsList);
					/*
					 * Aggiungo il DataRecord alla lista Di dataRecords
					 */
					dataRecordsList.add(dataRecord);
					/*
					 * Altrimenti se il formato del record non è valido
					 */
				} else {
					/*
					 * Aggiungo il record non corretto alla lista di recordsNonCorretti
					 */
					listaRecordsNonCorretti.add(records.get(i));
				}

			}

		} catch (IOException e) {
			System.err.println("File da leggere non trovato");
		} finally {
			try {
				leggiDocumento.close();
			} catch (IOException e) {
				System.err.println("Problema con la chiusura del File");
			}
		}
		/*
		 * Con la lista di dataRecords mi inizializzo un iteratore che mi servirà per
		 * scorrere la lista e tenerne conto per avere il record successivo
		 */
		iteratoreDataRecords = dataRecordsList.iterator();

		/*
		 * Se la lista dei recordsNonCorretti è vuota
		 */
		if (listaRecordsNonCorretti.isEmpty()) {
			/*
			 * Stampo a video che non ci sono records con formato non valido
			 */
			System.out.println("Non ci sono DataRecords con formato non valido");
			/*
			 * Altrimenti
			 */
		} else {
			/*
			 * Stampo a video i records non validi
			 */
			System.out.println("I Data Records che non hanno un formato valido sono:");
			for (int i = 0; i < listaRecordsNonCorretti.size(); i++) {
				System.out.println("-" + (i + 1) + " " + listaRecordsNonCorretti.get(i));
			}
		}
		System.out.println(" ");
	}

	/**
	 * Il metodo astratto mi rende la classe astratta. dal momento che l'unica cosa
	 * che cambia è il recuperare i dati nei due modi di leggere il file ed il resto
	 * è tutto uguale, faccio questo metodo astratto che deve essere implementato
	 * nelle singole classi figle
	 */
	protected abstract List<String> estraiDati(BufferedReader leggiDocumento) throws IOException;

	/**
	 * Mi restituisce il record successivo
	 */
	public DataRecord nextRecord() {
		if (iteratoreDataRecords.hasNext()) {
			return iteratoreDataRecords.next();
		} else {
			throw new NoSuchElementException("DataRecords finiti");
		}
	}

	/**
	 * Mi dice se esistono altri DataRecords
	 */
	public boolean hasNext() {
		if (iteratoreDataRecords.hasNext()) {
			return true;
		} else {
			return false;
		}
	}

}
